"""
Testing OberonFileReader with fake file system
"""

from contextlib import contextmanager
import os
import shutil

from pyfakefs.fake_filesystem_unittest import TestCase as FakeFSTestCase
from typing import Dict, Sequence, Optional

from otftp.file_io import OberonFileReader


class FileReaderTest(FakeFSTestCase):

    @contextmanager
    def create_tree(self, tree: Dict[Sequence, Optional[str]]):
        for file_path, content in tree.items():
            file_path = os.path.join(*file_path)
            # if content is None we create a directory
            # otherwise we create the parent directory
            # and the file with the given content
            if content is not None:
                dir, fname = os.path.split(file_path)
            else:
                dir = file_path
            if not os.path.exists(dir):
                os.makedirs(dir)
            if isinstance(content, str):
                mode = 'wt'
            else:
                mode = 'wb'
            if content is not None:
                with open(file_path, mode) as f:
                    f.write(content)
        try:
            yield
        finally:
            for file_path in tree.keys():
                file_path = os.path.join(*file_path)
                try:
                    # here we may attempt to delete several times the same directory
                    shutil.rmtree(file_path.split(os.sep)[0])
                except Exception as e:
                    print('miao')  # TODO FileNotFoundException?
                    pass


class OberonFileReaderTest(FileReaderTest):
    def setUp(self):
        self.setUpPyfakefs()
        self.files_dir = os.path.join('the', 'files', 'dir')
        self.file_name = 'my_file'
        self.b_file_name = self.file_name.encode('ascii')

    def test_file_in_files_dir(self):
        with self.create_tree({
            (self.files_dir, self.file_name): 'the content'
        }):
            OberonFileReader(self.files_dir, self.b_file_name)

    def test_file_not_in_files_dir(self):
        with self.assertRaises(FileNotFoundError) as ctx:
            OberonFileReader(self.files_dir, self.b_file_name)


class OberonFileReaderUnderOberonTest(FileReaderTest):
    def setUp(self):
        self.setUpPyfakefs()
        self.files_dir = os.path.join('the', 'Oberon', 'files', 'dir')
        self.file_name = 'my_file'
        self.b_file_name = self.file_name.encode('ascii')
        self.file_content = 'some file content'

    def test_file_in_files_dir(self):
        """ File in files_dir is supposed to be found """
        with self.create_tree({
            (self.files_dir, self.file_name): 'the content'
        }):
            OberonFileReader(self.files_dir, self.b_file_name)

    def test_file_in_oberon_dir(self):
        """ File in Oberon dir is not supposed to be found """
        with self.create_tree({
            ('the', 'Oberon', self.file_name): self.file_content
        }):
            with self.assertRaises(FileNotFoundError) as ctx:
                OberonFileReader(self.files_dir, self.b_file_name)

    def test_file_in_special_dir(self):
        """ File in Lib dir is supposed to be found """
        for special_dir in OberonFileReader.special_dirs:
            with self.subTest("file in special dir {}".format(special_dir)):
                with self.create_tree({
                    ('the', 'Oberon', special_dir, self.file_name): self.file_content
                }):
                    OberonFileReader(self.files_dir, self.b_file_name)

    def test_file_in_subdirectory_of_files_dir(self):
        """ File is in a subdirectory of files_dir, not supposed to be found """
        with self.create_tree({
            ('the', 'Oberon', 'subfolders', self.file_name): self.file_content
        }):
            with self.assertRaises(FileNotFoundError) as ctx:
                OberonFileReader(self.files_dir, self.b_file_name)
